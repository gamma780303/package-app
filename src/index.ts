import { ServiceActionsSchema, ServiceBroker, ServiceSchema} from 'moleculer';
import {Payload, ServiceConfig} from './interfaces';
import * as path from 'path';
import {ActionPayload, ActionResult, ServiceName} from 'package-types';
import * as fs from 'fs';
import recursiveRead from 'recursive-readdir';
import {Sequelize} from "sequelize-typescript";

export * from './interfaces';
export * from './types';

export class App {
    private broker: ServiceBroker;
    private static instance: App;
    private dbConnection?: Sequelize;
    private readonly config: ServiceConfig;

    constructor() {
        this.config = this.getConfig();
        this.broker = new ServiceBroker({ ...this.config.brokerConfig });
        if(this.config.dbConnection) {
            const { password, username, database, host, port, dialect } = this.config.dbConnection;
            this.dbConnection = new Sequelize(database, username, password, {host, port, dialect});
        }
    }

    public static getInstance(): App {
        if(!App.instance){
            App.instance = new App();
            process.on('SIGINT', App.instance.onShutDown);
            process.on('SIGQUIT', App.instance.onShutDown);
            process.on('SIGTERM', App.instance.onShutDown);

            process.on('uncaughtException', (err: any): void => {
                App.logError('Uncaught exception', err, { msgCode: 'UNCAUGHT_EXCEPTION' });
            });

            process.on('unhandledRejection', (err: any): void => {
                App.logError('Unhandled rejection', err, { msgCode: 'UNHANDLED_REJECTION' });
            });
        }
        return this.instance;
    }

    public async run(serviceSchema: ServiceSchema): Promise<void> {
        try {
            serviceSchema.actions = await this.initActions();
            this.broker.createService(serviceSchema);
            await this.broker.start();
        } catch (err) {
            App.logError(err);
            throw err;
        }
    }

    private getConfig(): ServiceConfig {
        const configPath = path.resolve(process.cwd(), 'dist/env/env.js');
        return require(configPath).config;
    }

    private async onShutDown(err: any): Promise<void> {
        if (err) {
            App.logError('Unexpected shutdown', err, { msgCode: 'UNEXPECTED_SHUTDOWN' });
        }
        App.logDebug('Stop service broker');
        await App.getInstance().broker.stop();
        await this.dbConnection?.close();

        setImmediate(() => process.exit(1));
    }

    public static async call<P, R>(serviceName: ServiceName, actionName: string, params: P): Promise<R> {
        const { broker } = App.getInstance();
        try {
            return broker.call(`${serviceName}.${actionName}`, params);
        } catch (err) {
            App.logError(`Error while calling ${serviceName}.${actionName}. Error: ${err}`);
            throw err;
        }
    }

    public static logDebug(...args: any[]) {
        App.getInstance().broker.logger.debug(...args);
    }

    public static logInfo(...args: any[]) {
        App.getInstance().broker.logger.info(...args);
    }

    public static logWarn(...args: any[]) {
        App.getInstance().broker.logger.warn(args);
    }

    public static logError(...args: any[]) {
        App.getInstance().broker.logger.error(...args);
    }

    public static logFatal(...args: any[]) {
        App.getInstance().broker.logger.fatal(...args);
    }

    public static logTrace(...args: any[]) {
        App.getInstance().broker.logger.trace(...args);
    }

    private async initActions(): Promise<ServiceActionsSchema> {
        try {
            const actionsPath = path.resolve(process.cwd(), 'dist/actions');
            if(!fs.existsSync(actionsPath)) {
                return {};
            }
            const files: string[] = path ? await recursiveRead(actionsPath, ['*.map']) : [];
            const actions: ServiceActionsSchema = {};
            files.forEach((fileName: string): void => {
                const action = require(path.resolve(fileName)).default;
                const schema = action.getValidationSchema();
                async function handler(ctx: Payload<ActionPayload>): Promise<ActionResult> {
                    App.logInfo(`Received call ${action.getName()}. Executing...`, ctx.params);
                    try {
                        const { currentUser } = ctx.params;
                        const result = await action.execute(ctx);
                        App.logInfo(`Received call ${action.getName()}. Done!`, result);
                        if(currentUser) {
                            result.currentUser = currentUser;
                        }
                        return result;
                    } catch (err) {
                        App.logError(`Error while executing action ${action.getName()}`, err);
                        throw err;
                    }

                }
                const name = action.getName();
                actions[name] = {
                    params: schema,
                    //@ts-ignore
                    handler
                }
                App.logInfo(`Action ${action.getName()} initialized`);
            });
            return actions;
        } catch (err) {
            App.logError(err);
            throw err;
        }
    }

    public getDBConnection(): Sequelize {
        if(this.dbConnection) {
            return this.dbConnection;
        } else throw new Error('No db here');
    }
}
