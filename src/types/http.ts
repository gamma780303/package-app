import { ServiceName, Role } from "package-types";

export enum HttpMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'
}

export interface Route {
    method: HttpMethod;
    path: string;
    role: Role;
    serviceName: ServiceName;
    actionName: string;
    middlewares: Function[];
}

export interface Aliases {
    [key: string]: string | Array<string | Function>
}
