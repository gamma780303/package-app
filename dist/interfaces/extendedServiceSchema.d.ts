import { ServiceSchema } from 'moleculer';
export interface ExtendedServiceSchema {
    moleculerServiceSchema: ServiceSchema;
    telegramToken?: string;
}
