import { ActionParams } from "moleculer";
export * from './config';
export interface Payload<ActionPayload extends {}> extends ActionParams {
    params: ActionPayload;
}
export interface Action {
    getName(): string;
    getValidationSchema(): any;
    execute(payload: Payload<any>): any;
}
