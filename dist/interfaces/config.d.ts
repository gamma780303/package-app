import { LogLevels } from "moleculer";
import { Dialect } from "sequelize";
export interface ServiceConfig {
    name: string;
    brokerConfig: BrokerConfig;
    http?: HttpConfig;
    dbConnection?: DBConnection;
    telegramToken?: string;
    accessTokenSecret?: string;
    refreshTokenSecret?: string;
    s3Config?: S3Config;
}
export interface BrokerConfig {
    transporter: string;
    nodeID: string;
    logger: boolean;
    logLevel: LogLevels;
}
export interface HttpConfig {
    port: number;
}
export interface DBConnection {
    database: string;
    username: string;
    password: string;
    host: string;
    port: number;
    dialect: Dialect;
}
export interface S3Config {
    s3Name: string;
    s3SecretKey: string;
    s3AccessKey: string;
    s3Region: string;
}
