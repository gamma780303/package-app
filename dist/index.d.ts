import { ServiceSchema } from 'moleculer';
import { ServiceName } from 'package-types';
import { Sequelize } from "sequelize-typescript";
export * from './interfaces';
export * from './types';
export declare class App {
    private broker;
    private static instance;
    private dbConnection?;
    private readonly config;
    constructor();
    static getInstance(): App;
    run(serviceSchema: ServiceSchema): Promise<void>;
    private getConfig;
    private onShutDown;
    static call<P, R>(serviceName: ServiceName, actionName: string, params: P): Promise<R>;
    static logDebug(...args: any[]): void;
    static logInfo(...args: any[]): void;
    static logWarn(...args: any[]): void;
    static logError(...args: any[]): void;
    static logFatal(...args: any[]): void;
    static logTrace(...args: any[]): void;
    private initActions;
    getDBConnection(): Sequelize;
}
