"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const moleculer_1 = require("moleculer");
const path = __importStar(require("path"));
const fs = __importStar(require("fs"));
const recursive_readdir_1 = __importDefault(require("recursive-readdir"));
const sequelize_typescript_1 = require("sequelize-typescript");
__exportStar(require("./interfaces"), exports);
__exportStar(require("./types"), exports);
class App {
    constructor() {
        this.config = this.getConfig();
        this.broker = new moleculer_1.ServiceBroker(Object.assign({}, this.config.brokerConfig));
        if (this.config.dbConnection) {
            const { password, username, database, host, port, dialect } = this.config.dbConnection;
            this.dbConnection = new sequelize_typescript_1.Sequelize(database, username, password, { host, port, dialect });
        }
    }
    static getInstance() {
        if (!App.instance) {
            App.instance = new App();
            process.on('SIGINT', App.instance.onShutDown);
            process.on('SIGQUIT', App.instance.onShutDown);
            process.on('SIGTERM', App.instance.onShutDown);
            process.on('uncaughtException', (err) => {
                App.logError('Uncaught exception', err, { msgCode: 'UNCAUGHT_EXCEPTION' });
            });
            process.on('unhandledRejection', (err) => {
                App.logError('Unhandled rejection', err, { msgCode: 'UNHANDLED_REJECTION' });
            });
        }
        return this.instance;
    }
    async run(serviceSchema) {
        try {
            serviceSchema.actions = await this.initActions();
            this.broker.createService(serviceSchema);
            await this.broker.start();
        }
        catch (err) {
            App.logError(err);
            throw err;
        }
    }
    getConfig() {
        const configPath = path.resolve(process.cwd(), 'dist/env/env.js');
        return require(configPath).config;
    }
    async onShutDown(err) {
        var _a;
        if (err) {
            App.logError('Unexpected shutdown', err, { msgCode: 'UNEXPECTED_SHUTDOWN' });
        }
        App.logDebug('Stop service broker');
        await App.getInstance().broker.stop();
        await ((_a = this.dbConnection) === null || _a === void 0 ? void 0 : _a.close());
        setImmediate(() => process.exit(1));
    }
    static async call(serviceName, actionName, params) {
        const { broker } = App.getInstance();
        try {
            return broker.call(`${serviceName}.${actionName}`, params);
        }
        catch (err) {
            App.logError(`Error while calling ${serviceName}.${actionName}. Error: ${err}`);
            throw err;
        }
    }
    static logDebug(...args) {
        App.getInstance().broker.logger.debug(...args);
    }
    static logInfo(...args) {
        App.getInstance().broker.logger.info(...args);
    }
    static logWarn(...args) {
        App.getInstance().broker.logger.warn(args);
    }
    static logError(...args) {
        App.getInstance().broker.logger.error(...args);
    }
    static logFatal(...args) {
        App.getInstance().broker.logger.fatal(...args);
    }
    static logTrace(...args) {
        App.getInstance().broker.logger.trace(...args);
    }
    async initActions() {
        try {
            const actionsPath = path.resolve(process.cwd(), 'dist/actions');
            if (!fs.existsSync(actionsPath)) {
                return {};
            }
            const files = path ? await (0, recursive_readdir_1.default)(actionsPath, ['*.map']) : [];
            const actions = {};
            files.forEach((fileName) => {
                const action = require(path.resolve(fileName)).default;
                const schema = action.getValidationSchema();
                async function handler(ctx) {
                    App.logInfo(`Received call ${action.getName()}. Executing...`, ctx.params);
                    try {
                        const { currentUser } = ctx.params;
                        const result = await action.execute(ctx);
                        App.logInfo(`Received call ${action.getName()}. Done!`, result);
                        if (currentUser) {
                            result.currentUser = currentUser;
                        }
                        return result;
                    }
                    catch (err) {
                        App.logError(`Error while executing action ${action.getName()}`, err);
                        throw err;
                    }
                }
                const name = action.getName();
                actions[name] = {
                    params: schema,
                    //@ts-ignore
                    handler
                };
                App.logInfo(`Action ${action.getName()} initialized`);
            });
            return actions;
        }
        catch (err) {
            App.logError(err);
            throw err;
        }
    }
    getDBConnection() {
        if (this.dbConnection) {
            return this.dbConnection;
        }
        else
            throw new Error('No db here');
    }
}
exports.App = App;
//# sourceMappingURL=index.js.map